from typing import List, Union

import functions.jobs.decode.main as job_decode
import functions.jobs.timestamp.main as timestamp_decode
import functions.jobs.upload_dcdb.main as job_upload_dcdb
import functions.jobs.deduplicate.main as job_algorithm_deduplicate
from functions.core.controller.src.cleanup import cleanup_pipeline_cache
from utils.data.deserialization import deserialize
from utils.decode.models import Algorithm
from utils.framework.pipeline import Pipeline, Event, Job
from utils.timestamp.models import TimestampedWiblObject


def handler(lambda_event: dict, _):
    event: Event = Event.from_dict(lambda_event)

    pipeline: Pipeline = event.pipeline
    source: str = event.source

    if len(pipeline.jobs) - 1 > pipeline.current_step_id:
        bump_next_step(pipeline=pipeline)
        return next(pipeline=pipeline)
    elif len(pipeline.jobs) - 1 < pipeline.current_step_id:
        raise IndexError("current step id outside range of jobs")

    # if job.init, queue decode and timestamping
    if source == "job.init":
        job_decode.register(pipeline=pipeline)
        timestamp_decode.register(pipeline=pipeline)
        bump_next_step(pipeline=pipeline)
    elif source == "job.timestamp":
        last_job: Job = pipeline.get_current_job()

        timestamped_wiblobject_serialized: bytes = pipeline.get_artifact(
            last_job, "data"
        )

        if timestamped_wiblobject_serialized is None:
            raise Exception("Unable to locate WiblObject.")

        # Deserialize serialized WiblObject dict
        timestamped_wiblobject_deserialized: dict = deserialize(
            timestamped_wiblobject_serialized
        )

        # Convert WiblObject-structured dict to WiblObject class
        timestamped_wiblobject: TimestampedWiblObject = TimestampedWiblObject.from_dict(
            timestamped_wiblobject_deserialized
        )

        algorithms: Union[
            List[Algorithm], None
        ] = timestamped_wiblobject.wiblobject.algorithms

        if len(algorithms) != 0:
            for algorithm in timestamped_wiblobject.wiblobject.algorithms:
                if algorithm.algorithm == "deduplication":
                    job_algorithm_deduplicate.register(pipeline=pipeline)
            bump_next_step(pipeline=pipeline)
        else:
            job_upload_dcdb.register(pipeline=pipeline)
            bump_next_step(pipeline=pipeline)
    elif source == "job.upload_dcdb":
        pipeline.next_step = "resolve"
        cleanup_pipeline_cache(bucket=pipeline.config.instance.storage.bucket_name, id=pipeline.id)
    else:
        job_upload_dcdb.register(pipeline=pipeline)
        bump_next_step(pipeline=pipeline)

    return next(pipeline=pipeline)


def next(pipeline: Pipeline) -> dict:
    return Event(pipeline=pipeline, source="controller").to_dict()


def bump_next_step(pipeline: Pipeline) -> None:
    pipeline.current_step_id += 1

    if len(pipeline.jobs) - 1 < pipeline.current_step_id:
        pipeline.next_step = "reject"
    else:
        pipeline.next_step = pipeline.jobs[pipeline.current_step_id].name
