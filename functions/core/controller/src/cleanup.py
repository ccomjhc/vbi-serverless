from utils.aws.s3 import delete_directory


def cleanup_pipeline_cache(bucket: str, id: str) -> None:
    response = delete_directory(bucket=bucket, key=f"cache/{id}")

    if response is False:
        raise Exception(f"Unable to cleanup cache of pipeline with id: '{id}'")
