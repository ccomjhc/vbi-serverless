from functions.core.prepare.src.cfg import put_instance_config, ResourcePutException
from functions.core.prepare.src.s3 import (
    require_bucket,
    require_bucket_directories,
    ResourceCreationException,
)


def handler(event, _):
    bucket_name = event["storage"]["bucket_name"]

    try:
        require_bucket(bucket_name)
        require_bucket_directories(bucket_name)
        put_instance_config(bucket_name, event)
    except ResourceCreationException or ResourcePutException as e:
        raise e
