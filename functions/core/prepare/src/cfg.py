import json
from typing import Any

from utils.aws.s3 import put_object


class ResourcePutException(Exception):
    pass


def put_instance_config(bucket: str, data: Any):
    response = put_object(bucket, "cfg/config.instance.json", json.dumps(data))

    if not response:
        raise ResourcePutException(
            "Unable to create or update instance configuration in S3."
        )
