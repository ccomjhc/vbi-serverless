import logging

import boto3

from utils.aws.s3 import key_exists, create_directory, create_bucket, bucket_exists


class ResourceCreationException(Exception):
    pass


class ResourceAccessException(Exception):
    pass


class ResourceConfigurationException(Exception):
    pass


logging.basicConfig(level=logging.INFO, format=" %(asctime)s %(levelname)s %(message)s")

DIRECTORIES = ["uploads/", "cache/", "cfg/"]


def require_bucket(bucket: str):
    response, code = bucket_exists(bucket)

    if response:
        logging.info(
            "Bucket already exists. If you are re-deploying the stack, this warning can be safely ignored."
        )
    else:
        if str(code) == "404":
            response = create_bucket(bucket)

            if not response:
                raise ResourceCreationException(
                    "Unable to create bucket '{0}'".format(bucket)
                )

            s3 = boto3.client("s3")

            response = s3.put_bucket_notification_configuration(
                Bucket=bucket,
                NotificationConfiguration={"EventBridgeConfiguration": {}},
            )

            if not response:
                raise ResourceConfigurationException(
                    "Unable to enable event bridge for bucket '{0}'".format(bucket)
                )
        elif str(code) == "403":
            raise ResourceAccessException(
                "Access forbidden to bucket '{0}'".format(bucket)
            )


def require_bucket_directories(bucket: str):
    for directory in DIRECTORIES:
        if not key_exists(bucket, directory):
            response = create_directory(bucket, directory)

            if not response:
                raise ResourceCreationException(
                    "Unable to create directory '{0}'".format(directory)
                )
