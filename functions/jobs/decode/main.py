from functions.jobs.decode.src.decode import decode_wiblfile
from utils.aws.s3 import S3ObjectDownloadException
from utils.data import serialization
from utils.decode.models import WiblObject
from utils.framework.pipeline import Pipeline, Version, Job, Event, Artifact


def handler(lambda_event: dict, _):
    event: Event = Event.from_dict(lambda_event)

    pipeline: Pipeline = event.pipeline

    last_job: Job = pipeline.get_last_job()
    current_job: Job = pipeline.get_current_job()

    response, path = pipeline.download_artifact(
        job=last_job, artifact_key="data", dest_path="/tmp/"
    )

    if not response:
        raise S3ObjectDownloadException("Unable to download WIBL data file.")

    wibl_data: WiblObject = decode_wiblfile(filename=path)

    serialized_wibl_data = serialization.serialize(wibl_data.to_dict())

    artifact: Artifact = Artifact(key="data", object_key="data.msgpack")
    pipeline.add_artifact(job=current_job, artifact=artifact, data=serialized_wibl_data)

    return Event(pipeline=pipeline, source="job.decode").to_dict()


def register(pipeline: Pipeline) -> Job:
    job: Job = Job(name="decode", version=Version(1, 0, 0))
    pipeline.register_job(job=job, has_config=False)
    return job
