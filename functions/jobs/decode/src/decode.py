import json
from datetime import datetime
from typing import Union, Any, List

from pynmea2 import nmea

from functions.jobs.decode.src.packets import PacketFactory
from functions.jobs.decode.src.timesource import determine_time_source
from utils.decode.models import (
    DataPacket,
    TimeSource,
    WiblObject,
    LoggerVersion,
    Algorithm,
    Metadata,
    SerializerVersion,
    JSONMetadata,
    AlgorithmRequest,
    SerialString,
)
from utils.statistics import PktStats, PktFaults


def decode_wiblfile(
    filename: str, max_reports: int = 1000, verbose: bool = False
) -> (WiblObject, PktStats):
    stats = PktStats(max_reports)
    packets: [DataPacket] = []
    needs_elapsed_time_fixup = False

    with open(filename, "rb") as file:
        source = PacketFactory(file)
        packet_num = 0

        while source.has_more():
            packet = source.next_packet()
            if packet is not None:
                if isinstance(packet, SerialString):
                    # We need to pull out the NMEA0183 recognition string
                    try:
                        name = packet.data[3:6].decode("utf-8")
                        stats.Observed(name)
                    except UnicodeDecodeError:
                        stats.Fault(name, PktFaults.DecodeFault)
                        continue
                else:
                    stats.Observed(packet.name())

                packet_num += 1

                if packet.elapsed == 0:
                    needs_elapsed_time_fixup = True

                if verbose and packet_num % 50000 == 0:
                    print(f"Reading file: passing {packet_num} packets ...")

                packets.append(packet)

    # We need some form of connection from elapsed time stamps (i.e., when the packet is received
    # at the logger) and a real time, so that we can interpolate to real-time information for all
    # the data.  There are a number of potential sources, including (in descending order of
    # preference), NMEA2000 SystemTime or GNSS, or NMEA0183 ZDA or RMC messages.  This code determines
    # which to use.
    time_source = determine_time_source(stats)

    # Under some circumstances, it is possible that packets will have no elapsed time in them
    # and therefore cannot be time-interpolated to a real time.  This generally is because the
    # source (i.e., from a non-WIBL logger) doesn't record elapsed times.  The best we can do in
    # this case is to fabricate an elapsed time based on whatever times we have, and then assign
    # an elapsed time to what's left based on the assumption that they had to have appeared at
    # some point between bordering timestamped data.  It's messy, but it's the best you're going
    # to get from loggers that don't record decent data ...
    if needs_elapsed_time_fixup:
        packets, stats = elapsed_time_fixup(packets, time_source, stats, verbose)

    return to_wiblobject(packets=packets, time_source=time_source)


def elapsed_time_fixup(packets, time_source, stats, verbose):
    if time_source == TimeSource.Time_ZDA or time_source == TimeSource.Time_RMC:
        # If we're using NMEA0183 strings for timing, then we have the possibility of there being
        # packets where there's no elapsed time stamp, and we need to unpack the NMEA string
        # for its real-time stamp, and then assign an elapsed time based on this.  This sets us up for
        # generating intermediate elapsed time estimates subsequently.
        realtime_elapsed_zero = None

        for n in range(len(packets)):
            if isinstance(packets[n], SerialString) and packets[n].elapsed == 0:
                # Decode the packet string to identify ZDA/RMC
                msg_id = packets[n].data[3:6].decode("utf-8")

                if (msg_id == "ZDA" and time_source == TimeSource.Time_ZDA) or (
                    msg_id == "RMC" and time_source == TimeSource.Time_RMC
                ):
                    if len(packets[n].data) < 11:
                        if verbose and stats.FaultCount(msg_id) < stats.fault_limit:
                            print(f"Error: short message {packets[n].data}; ignoring.")

                        stats.Fault(msg_id, PktFaults.ShortMessage)
                    else:
                        try:
                            msg = nmea.parse(packets[n].data.decode("utf-8"))
                            if msg.datestamp is not None and msg.timestamp is not None:
                                pkt_real_time = datetime.combine(
                                    msg.datestamp, msg.timestamp
                                )

                                if realtime_elapsed_zero is None:
                                    realtime_elapsed_zero = pkt_real_time
                                    packets[n].elapsed = 0
                                else:
                                    packets[n].elapsed = 1000.0 * (
                                        pkt_real_time.timestamp()
                                        - realtime_elapsed_zero.timestamp()
                                    )
                        except UnicodeDecodeError:
                            if verbose and stats.FaultCount(msg_id) < stats.fault_limit:
                                print(
                                    f"Error: unicode decode failure on NMEA string; ignoring."
                                )

                            stats.Fault(msg_id, PktFaults.DecodeFault)
                            continue
                        except nmea.ParseError:
                            if verbose and stats.FaultCount(msg_id) < stats.fault_limit:
                                print(
                                    f"Error: parse error in NMEA string {packets[n].data}; ignoring."
                                )

                            stats.Fault(msg_id, PktFaults.ParseFault)
                            continue
                        except TypeError:
                            if verbose and stats.FaultCount(msg_id) < stats.fault_limit:
                                print(
                                    f"Error: type error unpacking NMEA string; ignoring."
                                )

                            stats.Fault(msg_id, PktFaults.TypeFault)
                            continue
                if stats.FaultCount(msg_id) >= stats.fault_limit:
                    print(
                        f"Warning: too many errors on NMEA0183 packet {msg_id}; suppressing further reporting."
                    )

    # We now need to patch up any packets that don't have an elapsed time using the mean of the two nearest (ahead
    # and behind) packets with known elapsed times. The algorithm here finds pairs of elapsed times, and then fills
    # in all the packets with no elapsed times between them.
    oldest_position = None

    for n in range(len(packets)):
        if packets[n].elapsed == 0:
            if n > 0 and packets[n - 1].elapsed > 0:
                oldest_position = n - 1
        else:
            if oldest_position is not None:
                if n - oldest_position > 1:
                    # This means we hit the end of a potential bracket, so we need to process the elapsed times
                    target_elapsed_time = (
                        packets[oldest_position].elapsed + packets[n].elapsed
                    ) / 2.0

                    for i in range(n - oldest_position - 1):
                        packets[oldest_position + 1 + i].elapsed = target_elapsed_time

                    oldest_position = None

    # Finally, we replace any timestamps that weren't interpolated to make sure that there's no question that
    # they're not valid.
    for n in range(len(packets)):
        if packets[n].elapsed == 0:
            packets[n].elapsed = None

    return packets, stats


def to_wiblobject(packets: [DataPacket], time_source: TimeSource) -> WiblObject:
    logger_name: Union[str, None] = None
    platform: Union[str, None] = None
    logger_version: Union[LoggerVersion, None] = None
    metadata: Union[Any, None] = None
    algorithms: List[Algorithm] = list()

    for packet in packets:
        if isinstance(packet, Metadata):
            logger_name = packet.logger_name
            platform = packet.ship_name
        if isinstance(packet, SerializerVersion):
            logger_version = LoggerVersion(packet.major, packet.minor)
        if isinstance(packet, JSONMetadata):
            try:
                metadata = json.loads(packet.metadata)
            except ValueError:
                print(
                    f"Exception when converting JSON metadata string: '{packet.metadata}'"
                )
                continue
        if isinstance(packet, AlgorithmRequest):
            algorithms.append(
                Algorithm(algorithm=packet.algorithm, parameters=packet.parameters)
            )

    if not logger_name or not platform or not logger_version:
        raise Exception("Unable to construct WIBL data object")

    return WiblObject(
        logger_name=logger_name,
        platform=platform,
        logger_version=logger_version,
        metadata=metadata,
        algorithms=algorithms,
        packets=packets,
        time_source=time_source,
    )
