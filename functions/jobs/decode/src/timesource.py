from utils.decode.models import TimeSource, NoTimeSource
from utils.statistics import PktStats


# Work out which time source in the WIBL file should be used for real time associations
#
# WIBL files have an elapsed time associated with each packet that tags the time of reception
# of the packet according to the local oscillator (which might not be reliable, but at least
# is monotonic).  There are multiple potential ways to assign a correspondence between these
# timestamps and a real-world time, which depend on the types of packets that we have in the
# file.  This code selects which of several sources is likely to be best.
#
# \param stats  (PktStats) Statistics on which packets have been seen in the data
# \return TImeSource enum for the source of real-world time to use for referencing
def determine_time_source(stats: PktStats) -> TimeSource:
    """Work out which source of time can be used to provide the translation between
    elapsed time (local time-stamps that indicate a monotonic clock tick at the
    logger when the packet is received) to a real world time.  The preference is
    to use NMEA2000 system time packets, but then to attempt to use GNSS packets,
    and then finally to drop back to NMEA0183 ZDA or RMC packets (in that order)
    if required.

     Inputs:
         stats   (PktStats) Statistics on which packets have been seen in the data

     Outputs:
         TimeSource enum for which source should be used for timestamping
    """
    if stats.Seen("SystemTime"):
        res = TimeSource.Time_SysTime
    elif stats.Seen("GNSS"):
        res = TimeSource.Time_GNSS
    elif stats.Seen("ZDA"):
        res = TimeSource.Time_ZDA
    elif stats.Seen("RMC"):
        res = TimeSource.Time_RMC
    else:
        raise NoTimeSource()

    return res
