# We need to make sure that we aren't asked to process a version of the data format newer than we know
# (although we should accept anything older).  These define the latest version of the protocol that's
# understood by the code.

# Latest major protocol version understood by the code
protocol_version_major = 1
# Latest minor protocol version understood by the code
protocol_version_minor = 1


def protocol_version(major: int, minor: int) -> int:
    """Convert the version information for the serializer protocol to an integer so that we can do simple
    comparisons against what we find in the file being processed.

     Inputs:
         major   (int) Major version number for a protocol implementation
         minor   (int) Minor version number for a protocol implementation
     Output:
         (int) Combined (encoded) version indicator
    """
    return major * 1000 + minor


# Maximum protocol version understood by the code
maximum_version = protocol_version(protocol_version_major, protocol_version_minor)
