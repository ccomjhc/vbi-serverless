import numpy as np

from utils.deduplicate.models import AlgorithmLineageEntry, AlgorithmLineageEntryDetails
from utils.framework.config import Config
from utils.timestamp.models import TimestampedWiblObject


# Algorithm for delegating depth values in the provided data
# On some systems, it's possible to get duplicated depths from the echo sounder or control
# system (typically because the system is generating positions every second, and the depths
# aren't being updated that fast).  This algorithm does an exhaustive check through all
# the depths provided in sequence, and provides a boolean NumPy vector that provides an
# indication whether the depth is valid (True) or can be dropped (False).  The original data
# is not modified.
def find_duplicates(wiblobject: TimestampedWiblObject, config: Config) -> np.ndarray:
    current_depth = 0
    d = []
    for n in range(wiblobject.depth.z):
        if wiblobject.depth.z[n] != current_depth:
            d.append(n)
            current_depth = wiblobject.depth.z[n]
    rtn = np.array(d)
    if config.instance.verbose:
        n_ip_points = len(wiblobject.depth.z)
        n_op_points = len(rtn)
        print(
            f"After deduplication, total {n_op_points} points selected from {n_ip_points}"
        )
    return rtn


def deduplicate_depth(
    wiblobject: TimestampedWiblObject, config: Config
) -> TimestampedWiblObject:
    n_ip_points = len(wiblobject.depth.z)
    index = find_duplicates(wiblobject, config)
    wiblobject.depth.t = wiblobject.depth.t[index]
    wiblobject.depth.lat = wiblobject.depth.lat[index]
    wiblobject.depth.lon = wiblobject.depth.lon[index]
    wiblobject.depth.z = wiblobject.depth.z[index]

    # To memorialise that we did something, we add an entry to the lineage segment of
    # the metadata headers in the data.
    n_op_points = len(wiblobject.depth.t)
    wiblobject.lineage.append(
        AlgorithmLineageEntry(
            detail=AlgorithmLineageEntryDetails(
                name="deduplicate",
                parameters="",
                comment=f"Selected {n_op_points} non-duplicate depths from {n_ip_points} in input.",
            )
        )
    )

    return wiblobject
