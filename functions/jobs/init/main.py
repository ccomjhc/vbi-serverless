from utils.aws.s3 import get_object
from utils.framework.config import Config, load_config
from utils.framework.pipeline import Version, Pipeline, Job, Artifact, Event


def handler(event, _):
    bucket = event["detail"]["bucket"]["name"]
    key = event["detail"]["object"]["key"]

    config: Config = load_config(bucket=bucket)
    pipeline: Pipeline = Pipeline(config=config)

    job: Job = register(pipeline=pipeline)

    datafile: bytes = get_object(bucket=bucket, key=key)
    artifact: Artifact = Artifact(key="data", object_key="data.wibl")

    pipeline.add_artifact(job=job, artifact=artifact, data=datafile)

    return Event(pipeline=pipeline, source="job.init").to_dict()


def register(pipeline: Pipeline) -> Job:
    job: Job = Job(name="init", version=Version(1, 0, 0))
    pipeline.register_job(job=job, has_config=False)
    return job
