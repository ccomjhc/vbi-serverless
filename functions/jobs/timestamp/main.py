from functions.jobs.timestamp.src.timestamp import time_interpolation
from utils.data import serialization
from utils.data.deserialization import deserialize
from utils.decode.models import WiblObject
from utils.framework.pipeline import Event, Pipeline, Job, Version, Artifact
from utils.timestamp.models import TimestampedWiblObject


def handler(lambda_event: dict, _):
    event: Event = Event.from_dict(lambda_event)

    pipeline: Pipeline = event.pipeline

    last_job: Job = pipeline.get_last_job()
    current_job: Job = pipeline.get_current_job()

    wiblobject_serialized: bytes = pipeline.get_artifact(last_job, "data")

    if wiblobject_serialized is None:
        raise Exception("Unable to locate WiblObject.")

    # Deserialize serialized WiblObject dict
    wiblobject_deserialized: dict = deserialize(wiblobject_serialized)
    # Convert WiblObject-structured dict to WiblObject class
    wiblobject: WiblObject = WiblObject.from_dict(wiblobject_deserialized)

    # Create a TimestampedWiblObject class from WiblObject class
    timestamped_wiblobject: TimestampedWiblObject = time_interpolation(
        wiblobject, pipeline.config
    )

    # Serialize dict of TimestampedWiblObject class
    timestamped_wiblobject_serialized: bytes = serialization.serialize(
        timestamped_wiblobject.to_dict()
    )

    # Construct a new job artifact for serialized TimestampedWiblObject
    artifact: Artifact = Artifact(key="data", object_key="data.msgpack")

    # Save artifact to pipeline and post data to S3
    pipeline.add_artifact(
        job=current_job, artifact=artifact, data=timestamped_wiblobject_serialized
    )

    return Event(pipeline=pipeline, source="job.timestamp").to_dict()


def register(pipeline: Pipeline) -> Job:
    job: Job = Job(name="timestamp", version=Version(1, 0, 0))
    pipeline.register_job(job=job, has_config=True)
    return job
