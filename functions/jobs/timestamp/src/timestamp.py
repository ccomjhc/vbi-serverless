import datetime as dt

import pynmea2 as nmea

from utils.decode import models as decode
from utils.decode.models import WiblObject, TimeSource
from utils.framework.config import Config
from utils.interpolation.interpolation import InterpTable
from utils.statistics import PktStats, PktFaults
from utils.timestamp import models as timestamp

# Exception to indicate that there is no data to interpolate from the WIBL file
from utils.timestamp.models import TimestampedWiblObject


class NoData(Exception):
    pass


# Exception to indicate that the WIBL file is newer than the latest version understood by the code
class NewerDataFile(Exception):
    pass


# Construct a dictionary of interpolated observation data from a given WIBL file
#
# This carries out the basic read-convert-preprocess operations for a WIBL binary file, loading in all
# the packets, fixing up any missing elapsed timestamps on the packets using real-world indicators
# in the packets, and then assigning real-world times to all the recorded observable data packets,
# interpolating the positioning information at those points to give a (time, position, observation) set
# for each observation data set.  The data are encoded into a dictionary with auxiliary information
# such as metadata identifying the logger, required processing algorithms, etc.
def time_interpolation(wiblobject: WiblObject, config: Config) -> TimestampedWiblObject:
    fault_limit = config.job.get("timestamp").get("fault_limit")
    verbose = config.instance.verbose
    elapsed_time_quantum = config.job.get("timestamp").get("elapsed_time_quantum")

    time_table = InterpTable(
        [
            "ref",
        ]
    )  # Mapping of elapsed time to real-world time
    position_table = InterpTable(
        ["lon", "lat"]
    )  # Mapping of elapsed time to position information
    depth_table = InterpTable(
        [
            "z",
        ]
    )  # Mapping of elapsed time to depth
    hdg_table = InterpTable(
        [
            "h",
        ]
    )  # Mapping of elapsed time to heading information
    wattemp_table = InterpTable(
        [
            "temp",
        ]
    )  # Mapping of elapsed time to water temperature
    wind_table = InterpTable(
        ["dir", "spd"]
    )  # Mapping of elapsed time to wind direction and speed

    # Reset statistics so that we don't double count on the second pass
    stats = PktStats(fault_limit)

    # packet stats is empty by default, what would it find for a time source?
    time_source = wiblobject.time_source

    seconds_per_day = 24.0 * 60.0 * 60.0

    elapsed_offset = 0  # Estimate of the offset in milliseconds to add to elapsed times recorded (if counter lapsed)
    last_elapsed = 0  # Marker for the last observed elapsed time (to check for lapping the counter)

    for packet in wiblobject.packets:
        # After this point, any packet that we're interested in has to have an elapsed time assigned
        if packet.elapsed is None:
            continue

        # We need to check that the elapsed ms counter hasn't wrapped around since
        # the last packet.  If so, we need to increment the elapsed time base stamp.
        # This method is very simple, and will fail mightily if the elapsed times in
        # the log file are not sequential and monotonic (modulo the elapsed_time_quantum).
        if packet.elapsed < last_elapsed:
            elapsed_offset = elapsed_offset + elapsed_time_quantum

        last_elapsed = packet.elapsed

        if isinstance(packet, decode.SystemTime):
            stats.Observed(packet.name())

            if time_source == TimeSource.Time_SysTime:
                time_table.add_point(
                    packet.elapsed + elapsed_offset,
                    "ref",
                    packet.date * seconds_per_day + packet.timestamp,
                )
        if isinstance(packet, decode.Depth):
            stats.Observed(packet.name())

            depth_table.add_point(packet.elapsed + elapsed_offset, "z", packet.depth)
        if isinstance(packet, decode.GNSS):
            stats.Observed(packet.name())

            if time_source == TimeSource.Time_GNSS:
                time_table.add_point(
                    packet.elapsed + elapsed_offset,
                    "ref",
                    packet.date * seconds_per_day + packet.timestamp,
                )
            position_table.add_points(
                packet.elapsed + elapsed_offset,
                ["lat", "lon"],
                [packet.latitude, packet.longitude],
            )

        packet_name = packet.name()

        print(packet_name)

        if isinstance(packet, decode.SerialString):
            try:
                stats.Observed(packet_name)
                data = packet.data.decode("UTF-8")
                if stats.FaultCount(packet_name) == stats.fault_limit:
                    print(
                        f"Warning: too many errors on packet {packet_name}; supressing further reporting."
                    )
                if len(data) > 11:
                    try:
                        msg = nmea.parse(data)
                        if (
                            isinstance(msg, nmea.ZDA)
                            and time_source == TimeSource.Time_ZDA
                        ):
                            if msg.datestamp is not None and msg.timestamp is not None:
                                reftime = dt.datetime.combine(
                                    msg.datestamp, msg.timestamp
                                )
                                time_table.add_point(
                                    packet.elapsed + elapsed_offset,
                                    "ref",
                                    reftime.timestamp(),
                                )
                        if (
                            isinstance(msg, nmea.RMC)
                            and time_source == TimeSource.Time_RMC
                        ):
                            if msg.datestamp is not None and msg.timestamp is not None:
                                reftime = dt.datetime.combine(
                                    msg.datestamp, msg.timestamp
                                )
                                time_table.add_point(
                                    packet.elapsed + elapsed_offset,
                                    "ref",
                                    reftime.timestamp(),
                                )
                        if isinstance(msg, nmea.GGA) or isinstance(msg, nmea.GLL):
                            if msg.latitude is not None and msg.longitude is not None:
                                position_table.add_points(
                                    packet.elapsed + elapsed_offset,
                                    ["lat", "lon"],
                                    [msg.latitude, msg.longitude],
                                )
                        if isinstance(msg, nmea.DBT):
                            if msg.depth_meters is not None:
                                depth: float = float(msg.depth_meters)

                                depth_table.add_point(
                                    packet.elapsed + elapsed_offset, "z", depth
                                )
                        if isinstance(msg, nmea.HDT):
                            if msg.heading is not None:
                                heading: float = float(msg.heading)

                                hdg_table.add_point(
                                    packet.elapsed + elapsed_offset, "h", heading
                                )
                        if isinstance(msg, nmea.MWD):
                            if (
                                msg.direction_true is not None
                                and msg.wind_speed_meters is not None
                            ):
                                direction: float = float(msg.direction_true)
                                speed: float = float(msg.wind_speed_meters)

                                wind_table.add_points(
                                    packet.elapsed + elapsed_offset,
                                    ["dir", "spd"],
                                    [direction, speed],
                                )
                        if isinstance(msg, nmea.MTW):
                            if msg.temperature is not None:
                                temperature: float = float(msg.temperature)

                                wattemp_table.add_point(
                                    packet.elapsed + elapsed_offset, "temp", temperature
                                )
                    except nmea.ChecksumError as e:
                        if (
                            verbose
                            and stats.FaultCount(packet_name) < stats.fault_limit
                        ):
                            print(f"Checksum error: {e}")
                        stats.Fault(packet_name, PktFaults.ChecksumFault)
                        continue
                    except nmea.ParseError as e:
                        if (
                            verbose
                            and stats.FaultCount(packet_name) < stats.fault_limit
                        ):
                            print(f"Parse error: {e}")
                        stats.Fault(packet_name, PktFaults.ParseFault)
                        continue
                    except AttributeError as e:
                        if (
                            verbose
                            and stats.FaultCount(packet_name) < stats.fault_limit
                        ):
                            print(f"Attribute error: {e}")
                        stats.Fault(packet_name, PktFaults.AttributeFault)
                        continue
                    except TypeError as e:
                        if (
                            verbose
                            and stats.FaultCount(packet_name) < stats.fault_limit
                        ):
                            print(f"Type error: {e}")
                        stats.Fault(packet_name, PktFaults.TypeFault)
                        continue
                else:
                    # Packets have to be at least 11 characters to contain all the mandatory elements.
                    # Usually a short packet is broken in some fashion, and should be ignored.
                    if verbose and stats.FaultCount(packet_name) < stats.fault_limit:
                        print(f"Error: short message: {data}; ignoring.")
                    stats.Fault(packet_name, PktFaults.ShortMessage)
            except UnicodeDecodeError as e:
                if verbose and stats.FaultCount(packet_name) < stats.fault_limit:
                    print(f"Decode error: {e}")
                stats.Fault(packet_name, PktFaults.DecodeFault)
                continue
    if verbose:
        print("Reference time table length = ", time_table.n_points())
        print("Position table length = ", position_table.n_points())
        print("Depth observations = ", depth_table.n_points())
        print(stats)

    if depth_table.n_points() < 1:
        raise NoData()

    # Finally, do the interpolations to generate the output data, and package it up as a dictionary for return
    depth_timepoints = depth_table.ind()
    z_times = time_table.interpolate(["ref"], depth_timepoints)[0]
    z_lat, z_lon = position_table.interpolate(["lat", "lon"], depth_timepoints)

    hdg_timepoints = hdg_table.ind()
    hdg_times = time_table.interpolate(["ref"], hdg_timepoints)[0]
    hdg_lat, hdg_lon = position_table.interpolate(["lat", "lon"], hdg_timepoints)

    wt_timepoints = wattemp_table.ind()
    wt_times = time_table.interpolate(["ref"], wt_timepoints)[0]
    wt_lat, wt_lon = position_table.interpolate(["lat", "lon"], wt_timepoints)

    wind_timepoints = wind_table.ind()
    wind_times = time_table.interpolate(["ref"], wind_timepoints)[0]
    wind_lat, wind_lon = position_table.interpolate(["lat", "lon"], wind_timepoints)

    depth: timestamp.Depth = timestamp.Depth(
        t=z_times, lat=z_lat, lon=z_lon, z=depth_table.var("z")
    )
    heading: timestamp.Heading = timestamp.Heading(
        t=hdg_times, lat=hdg_lat, lon=hdg_lon, heading=hdg_table.var("h")
    )
    water_temperature: timestamp.WaterTemperature = timestamp.WaterTemperature(
        t=wt_times, lat=wt_lat, lon=wt_lon, temperature=wattemp_table.var("temp")
    )
    wind: timestamp.Wind = timestamp.Wind(
        t=wind_times,
        lat=wind_lat,
        lon=wind_lon,
        direction=wind_table.var("dir"),
        speed=wind_table.var("spd"),
    )

    return TimestampedWiblObject(
        wiblobject=wiblobject,
        depth=depth,
        heading=heading,
        water_temperature=water_temperature,
        wind=wind,
    )
