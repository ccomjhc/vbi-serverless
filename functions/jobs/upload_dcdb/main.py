import json

from functions.jobs.upload_dcdb.src.transmit import transmit_geojson
from utils.data.deserialization import deserialize
from utils.framework.pipeline import Event, Pipeline, Job, Version, Artifact
from utils.timestamp.models import TimestampedWiblObject
from utils.transform.geojson import to_geojson


def handler(lambda_event: dict, _):
    event: Event = Event.from_dict(lambda_event)

    pipeline: Pipeline = event.pipeline

    last_job: Job = pipeline.get_last_job()
    current_job: Job = pipeline.get_current_job()

    timestamped_wiblobject_serialized: bytes = pipeline.get_artifact(last_job, "data")

    if timestamped_wiblobject_serialized is None:
        raise Exception("Unable to locate WiblObject.")

    # Deserialize serialized WiblObject dict
    timestamped_wiblobject_deserialized: dict = deserialize(
        timestamped_wiblobject_serialized
    )

    # Convert WiblObject-structured dict to WiblObject class
    timestamped_wiblobject: TimestampedWiblObject = TimestampedWiblObject.from_dict(
        timestamped_wiblobject_deserialized
    )

    timestamped_wiblobject_geojson = to_geojson(
        timestamped_wiblobject=timestamped_wiblobject,
        provider_id=pipeline.config.job.get("upload_dcdb").get("dcdb_provider_id"),
    )

    response = transmit_geojson(
        source_id=pipeline.id,
        data=json.dumps(timestamped_wiblobject_geojson).encode("utf-8"),
        config=pipeline.config,
    )

    if response is None:
        print(
            f"Failure occurred during transfer of artifact '{last_job.artifacts.get('data').key}' to DBDB."
        )
    else:
        print(
            f"Successfully transferred artifact '{last_job.artifacts.get('data').key}' to DBDB."
        )

    artifact: Artifact = Artifact(key="data", object_key="data.msgpack")

    pipeline.add_artifact(
        job=current_job, artifact=artifact, data=timestamped_wiblobject_serialized
    )

    return Event(pipeline=pipeline, source="job.upload_dcdb").to_dict()


def register(pipeline: Pipeline) -> Job:
    job: Job = Job(name="upload_dcdb", version=Version(1, 0, 0))
    pipeline.register_job(job=job, has_config=True)
    return job
