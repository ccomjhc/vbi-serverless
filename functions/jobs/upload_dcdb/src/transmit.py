import json

import requests as requests

from utils.framework.config import Config


# Send a specified GeoJSON dictionary to the user's specified end-point
#
# Manage the process for formatting the POST request to DCDB's submission API, transmitting the file into
# the DCDB incoming buffers.  Among other things, this makes sure that the provider identification is
# embedded in the unique identifier (since DCDB uses this to work out where to route the data). A local
# mode is also provided, so you can test the upload without having to be connected, or annoy DCDB.
#
# \param source_id          Unique ID encoded into the GeoJSON metadata
# \param data               Filename for the local GeoJSON file to transmit
# \param config             Configuration dictionary
# \return True if the upload succeeded, otherwise False
def transmit_geojson(source_id: str, data: bytes, config: Config) -> bool:
    provider_id: str = config.job.get("upload_dcdb").get("dcdb_provider_id")
    provider_token: str = config.job.get("upload_dcdb").get("dcdb_provider_token")

    # The unique ID that we provide to DCDB must be preceded by our provider ID and a hyphen.
    # This should happen before the code gets to here, but just in case, we enforce this requirement.
    if provider_id not in source_id:
        dest_id = provider_id + "-" + source_id
    else:
        dest_id = source_id

    if config.instance.verbose:
        print(
            f"Source ID is: {source_id}; Destination object ID is: {dest_id}; Authorization token is: {provider_token}"
        )

    files = {
        "file": (source_id, data),
        "metadataInput": (None, json.dumps({"uniqueID": dest_id})),
    }

    endpoint: str = config.job.get("upload_dcdb").get("dcdb_endpoint")
    headers = {"x-auth-token": provider_token}

    if config.instance.verbose:
        print(
            f"Transmitting for source ID {source_id} to {endpoint} as destination ID {dest_id}."
        )

    response = requests.post(endpoint, headers=headers, files=files)
    json_response = response.json()

    print(f"POST response is {json_response}")

    try:
        rc = json_response["success"]
        if not rc:
            rc = None
    except json.decoder.JSONDecodeError:
        rc = None

    return rc
