boto3~=1.21.28
botocore~=1.24.28
msgpack~=1.0.3
mashumaro~=3.0.1
pynmea2~=1.18.0
numpy~=1.22.3
requests~=2.27.1