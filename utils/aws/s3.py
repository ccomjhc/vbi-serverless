import logging
from typing import Any, Union

import boto3
from botocore.exceptions import ClientError

logging.basicConfig(level=logging.INFO, format=" %(asctime)s %(levelname)s %(message)s")


class ObjectGetException(Exception):
    pass


class S3ObjectCreationException(Exception):
    pass


class S3ObjectDownloadException(Exception):
    pass


class S3ObjectPutException(Exception):
    pass


def bucket_exists(bucket: str) -> (bool, Union[int, None]):
    s3 = boto3.client("s3")

    try:
        s3.head_bucket(Bucket=bucket)
    except ClientError as e:
        return False, e.response["Error"]["Code"]

    return True, None


def create_bucket(bucket: str) -> bool:
    s3 = boto3.client("s3")

    try:
        response = s3.create_bucket(Bucket=bucket)
        logging.info(
            "Created {0}, HTTPStatusCode: {1}".format(
                bucket, response["ResponseMetadata"]["HTTPStatusCode"]
            )
        )
        return True
    except ClientError as e:
        logging.error("Received error: {0}".format(e), exc_info=True)
        return False


def key_exists(bucket: str, key: str) -> bool:
    s3 = boto3.client("s3")
    response = s3.list_objects_v2(Bucket=bucket, Prefix=key)

    if response and response["KeyCount"] > 0:
        for obj in response["Contents"]:
            if key == obj["Key"]:
                return True

    return False


def create_directory(bucket: str, key: str) -> bool:
    if not key.endswith("/"):
        return False

    s3 = boto3.client("s3")

    try:
        response = s3.put_object(Bucket=bucket, Key=key)
        logging.info(
            "Created {0}, HTTPStatusCode: {1}".format(
                key, response["ResponseMetadata"]["HTTPStatusCode"]
            )
        )
        return True
    except ClientError as e:
        logging.error("Received error: {0}".format(e), exc_info=True)
        return False


def download_file(bucket: str, key: str, dest_path: str) -> [bool, Union[str, None]]:
    # reject if specified key is declared in the format of a directory
    if not dest_path.startswith("/") and not dest_path.endswith("/"):
        return False, None

    s3 = boto3.client("s3")

    basename = key.split("/")[-1].strip()
    dest = dest_path + basename

    try:
        s3.download_file(Bucket=bucket, Key=key, Filename=dest)
        return True, dest
    except ClientError as e:
        logging.error("Received error: {0}".format(e), exc_info=True)
        return False, None


def get_object(bucket: str, key: str) -> Union[bytes, None]:
    s3 = boto3.client("s3")

    try:
        response = s3.get_object(Bucket=bucket, Key=key)
        return response["Body"].read()
    except ClientError as e:
        logging.error("Received error: {0}".format(e), exc_info=True)
        return None


def put_object(bucket: str, key: str, data: Any) -> bool:
    s3 = boto3.client("s3")

    try:
        response = s3.put_object(Bucket=bucket, Key=key, Body=data)
        logging.info(
            "Object put {0}, HTTPStatusCode: {1}".format(
                bucket, response["ResponseMetadata"]["HTTPStatusCode"]
            )
        )
        return True
    except ClientError as e:
        logging.error("Received error: {0}".format(e), exc_info=True)
        return False


def delete_directory(bucket: str, key: str) -> bool:
    s3 = boto3.client("s3")
    response = s3.list_objects_v2(Bucket=bucket, Prefix=key)

    try:
        for entry in response["Contents"]:
            s3.delete_object(Bucket=bucket, Key=entry["Key"])
        return True
    except ClientError as e:
        logging.error("Received error: {0}".format(e), exc_info=True)
        return False
