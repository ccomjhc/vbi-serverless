from typing import Any

import msgpack


def deserialize(data: Any):
    return msgpack.unpackb(data)
