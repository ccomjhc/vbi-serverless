from typing import Any

import msgpack


def serialize(data: Any) -> [bytes, None]:
    return msgpack.packb(data)
