# Convert from Kelvin to degrees Celsius
#
# Temperature is stored in the NMEA2000 packets as Kelvin, but that isn't terribly useful for end users. This converts
# into degrees Celsius so that output is more usable.
#
# \param temp   Temperature in Kelvin
# \return Temperature in degrees Celsius
def temp_to_celsius(temp):
    return temp - 273.15


# Convert from Pascals to millibars
#
# Pressure is stored in the NMEA2000 packets as Pascals, but that isn't terribly useful for end users. This converts
# into millibars so that output is more usable.
#
# \param pressure   Pressure in Pascals
# \return Pressure in millibars
def pressure_to_millibars(pressure):
    return pressure / 100.0


# Convert from radians to degrees
#
# Angles are stored in the NMEA2000 packets as radians, but that isn't terribly useful for end users (at least for
# display). This converts into degrees so that output is more usable.
#
# \param rads   Angle in radians
# \return Angle in degrees
def angle_to_degrees(rads):
    return rads * 180.0 / 3.1415926535897932384626433832795
