from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from enum import Enum
from typing import Dict, List, Union, Tuple, Any

from mashumaro.mixins.json import DataClassJSONMixin

from utils.decode.conversions import (
    angle_to_degrees,
    temp_to_celsius,
    pressure_to_millibars,
)
from utils.models import LineageEntry

# Encapsulate the types of real-world time information that can be used
#
# In order to establish the relationship between the elapsed time (i.e., time of reception) of
# each packet and the real-world time, we need to use the encoded timestamps in one of the
# packets in the file.  There are a number of choices for this, depending on what type of
# data we have in there; this enumeration provides a controlled list of the options.
class TimeSource(Enum):
    # Use NMEA2000 packet for SystemTime
    Time_SysTime = 0
    # Use NMEA2000 packet from GNSS observations
    Time_GNSS = 1
    # Use NMEA0183 ZDA timestamp packets
    Time_ZDA = 2
    # Use NMEA0183 RMC minimum data with timestamps
    Time_RMC = 3


# Exception to report that no adequate source of real-world time information is available
class NoTimeSource(Exception):
    pass


@dataclass
class Algorithm(DataClassJSONMixin):
    algorithm: str = None
    parameters: str = None


@dataclass
class LoggerVersion(DataClassJSONMixin):
    major: int = None
    minor: int = None


def deserialize_packets(data_packets: Dict[Any, Any]) -> List["Packet"]:
    packets: List[Packet] = []

    for packet in data_packets:
        packet_id: int = packet["packet_id"]

        if packet_id == PacketTypes.SerializerVersion.value.get("id"):
            packets.append(SerializerVersion.from_dict(packet))
        elif packet_id == PacketTypes.SystemTime.value.get("id"):
            packets.append(SystemTime.from_dict(packet))
        elif packet_id == PacketTypes.Attitude.value.get("id"):
            packets.append(Attitude.from_dict(packet))
        elif packet_id == PacketTypes.Depth.value.get("id"):
            packets.append(Depth.from_dict(packet))
        elif packet_id == PacketTypes.COG.value.get("id"):
            packets.append(COG.from_dict(packet))
        elif packet_id == PacketTypes.GNSS.value.get("id"):
            packets.append(GNSS.from_dict(packet))
        elif packet_id == PacketTypes.Environment.value.get("id"):
            packets.append(Environment.from_dict(packet))
        elif packet_id == PacketTypes.Temperature.value.get("id"):
            packets.append(Temperature.from_dict(packet))
        elif packet_id == PacketTypes.Humidity.value.get("id"):
            packets.append(Humidity.from_dict(packet))
        elif packet_id == PacketTypes.Pressure.value.get("id"):
            packets.append(Pressure.from_dict(packet))
        elif packet_id == PacketTypes.SerialString.value.get("id"):
            packets.append(SerialString.from_dict(packet))
        elif packet_id == PacketTypes.Motion.value.get("id"):
            packets.append(Motion.from_dict(packet))
        elif packet_id == PacketTypes.Metadata.value.get("id"):
            packets.append(Metadata.from_dict(packet))
        elif packet_id == PacketTypes.AlgorithmRequest.value.get("id"):
            packets.append(AlgorithmRequest.from_dict(packet))
        elif packet_id == PacketTypes.JSONMetadata.value.get("id"):
            packets.append(JSONMetadata.from_dict(packet))
        elif packet_id == PacketTypes.NMEA0183Filter.value.get("id"):
            packets.append(NMEA0183Filter.from_dict(packet))
        else:
            print(f"Unknown packet with ID {packet_id} in input stream; ignored.")
            continue

    return packets


class PacketTypes(Enum):
    # Version information for the logger's file construction code, and the NMEA2000 and NMEA0183 loggers
    SerializerVersion = {"id": 0, "name": "SerializerVersion"}
    # NMEA2000 SystemTime information
    SystemTime = {"id": 1, "name": "SystemTime"}
    # NMEA2000 Attitude (roll, pitch, yaw) information
    Attitude = {"id": 2, "name": "Attitude"}
    # NMEA2000 Depth information
    Depth = {"id": 3, "name": "Depth"}
    # NMEA2000 Course-over-ground information
    COG = {"id": 4, "name": "COG"}
    # NMEA2000 GNSS report information
    GNSS = {"id": 5, "name": "GNSS"}
    # NMEA2000 Environmental (temperature, pressure, and humidity) information
    Environment = {"id": 6, "name": "Environment"}
    # NMEA2000 Temperature information
    Temperature = {"id": 7, "name": "Temperature"}
    # NMEA2000 Humidity information
    Humidity = {"id": 8, "name": "Humidity"}
    # NMEA2000 Pressure information
    Pressure = {"id": 9, "name": "Pressure"}
    # Encapsulated NMEA0183 serial sentence
    SerialString = {"id": 10, "name": "SerialString"}
    # Local motion sensor (three-axis acceleration, three-axis gyro) information
    Motion = {"id": 11, "name": "Motion"}
    # Logger and ship identification information used for construction GeoJSON metadata on output
    Metadata = {"id": 12, "name": "Metadata"}
    # Requests for algorithms to be run on the data in post-processing
    AlgorithmRequest = {"id": 13, "name": "AlgorithmRequest"}
    # Arbitrary JSON metadata string used to fill in platform-specific items in the GeoJSON metadata on output
    JSONMetadata = {"id": 14, "name": "JSONMetadata"}
    # Specification for a NMEA0183 packet to be recorded at the logger
    NMEA0183Filter = {"id": 15, "name": "NMEA0183Filter"}


# Base class for all data packets that can be read from the binary file
#
# This provides a common base class for all the data packets, and stores the information on the date and time at
# which the packet was received.
@dataclass
class DataPacket(ABC):
    # Packet ID number
    packet_id: int = None
    # Date in days since 1970-01-01
    date: int = 0
    # Time in seconds since midnight on the day in question
    timestamp: float = 0.0
    # Time in milliseconds since boot (reference time)
    elapsed: int = 0

    # Abstract method for constructing the payload of the packet for serialisation
    #
    # This builds a buffer of the data required for the data packet so that the code can then serialise
    # it in new files.
    # @abstractmethod
    # def payload(self) -> bytes:
    #     pass

    # Abstract method for fetching the human-readable name associated with the packet
    @staticmethod
    @abstractmethod
    def name() -> str:
        pass

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for the standard streaming output interface.
    #
    # \param self   Pointer to the object
    # \return String representation of the object
    def __str__(self):
        return (
            "["
            + str(self.date)
            + " days, "
            + str(self.timestamp)
            + " s., "
            + str(self.elapsed)
            + " ms elapsed]"
        )


#  Unpack the serializer version information packet, and store versions
#
# This picks apart the information on the version of the serializer used to generate the file being read. This should
# always be the first packet in the file, and allows the code to adjust readers if necessary in order to read what's
# coming next.
@dataclass
class SerializerVersion(DataPacket, DataClassJSONMixin):
    # Major software version for the serializer code
    major: int = None
    # Minor software version for the serializer code
    minor: int = None
    # A tuple of the NMEA2000 software version
    nmea2000: Tuple[int, int, int] = None
    # A tuple of the NMEA0183 software version
    nmea0183: Tuple[int, int, int] = None

    packet_id: int = PacketTypes.SerializerVersion.value.get("id")

    # NMEA2000 software version information
    # todo: document
    @classmethod
    def get_nmea2000_version(cls):
        return (
            str(cls.nmea2000[0])
            + "."
            + str(cls.nmea2000[1])
            + "."
            + str(cls.nmea2000[2])
        )  # NMEA0183 software version information

    # todo: document
    @classmethod
    def get_nmea0183_version(cls):
        return (
            str(cls.nmea0183[0])
            + "."
            + str(cls.nmea0183[1])
            + "."
            + str(cls.nmea0183[2])
        )  # Provide the fixed-text string name for this data packet

    #
    # This simply reports the human-readable name for the class so that reporting is possible
    #
    # \return String with the human-readable name of the packet
    @staticmethod
    def name():
        return PacketTypes.SerializerVersion.value.get("name")

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for the standard streaming output interface.
    #
    # \param self   Pointer to the object
    # \return String representation of the object
    def __str__(self):
        return (
            super().__str__()
            + " "
            + self.name()
            + f": version = {self.major}.{self.minor}, with NMEA2000 version {self.nmea2000} and NMEA0183 version "
            f"{self.nmea0183} "
        )


# Implementation of the SystemTime NMEA2000 packet
#
# This retrieves the timestamp, logger elapsed time, and time source for a SystemTime packet serialised into the file.
@dataclass
class SystemTime(DataPacket, DataClassJSONMixin):
    # Source of the timestamp (see documentation for decoding, but at least GNSS)
    data_source: int = None

    packet_id: int = PacketTypes.SystemTime.value.get("id")

    # Provide the fixed-text string name for this data packet
    #
    # This simply reports the human-readable name for the class so that reporting is possible
    #
    # \return String with the human-readable name of the packet
    @staticmethod
    def name():
        return PacketTypes.SystemTime.value.get("name")

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for the standard streaming output interface.
    #
    # \param self   Pointer to the object
    # \return String representation of the object
    def __str__(self):
        return (
            DataPacket.__str__(self)
            + " "
            + self.name()
            + ": source = "
            + str(self.data_source)
        )  # Implementation of the Attitude NMEA2000 packet


#
# The attitude message contains estimates of roll, pitch, and yaw of the ship, without any indication of where the data
# is coming from.  Consequently, the data is just reported directly.
@dataclass
class Attitude(DataPacket, DataClassJSONMixin):
    # Yaw angle of the ship, radians (+ve clockwise from north)
    yaw: float = None
    # Pitch angle of the ship, radians (+ve bow up)
    pitch: float = None
    # Roll angle of the ship, radians (+ve port up)
    roll: float = None

    packet_id: int = PacketTypes.SystemTime.value.get("id")

    # Provide the fixed-text string name for this data packet
    #
    # This simply reports the human-readable name for the class so that reporting is possible
    #
    # \return String with the human-readable name of the packet
    @staticmethod
    def name():
        return PacketTypes.SystemTime.value.get("name")

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for the standard streaming output interface.
    #
    # \param self   Pointer to the object
    # \return String representation of the object
    def __str__(self):
        return (
            DataPacket.__str__(self)
            + " "
            + self.name()
            + ": yaw = "
            + str(angle_to_degrees(self.yaw))
            + " deg, pitch = "
            + str(angle_to_degrees(self.pitch))
            + " deg, roll = "
            + str(angle_to_degrees(self.roll))
            + " deg"
        )


# Implement the Observed Depth NMEA2000 message
#
# The depth message includes the observed depth, the offset that needs to be applied to it either for rise from the keel
# or waterline, and the maximum depth that can be observed (allowing for some filtering).
@dataclass
class Depth(DataPacket, DataClassJSONMixin):
    # Observed depth below transducer, metres
    depth: float = None
    # Offset for depth, metres.
    # This is an offset to apply to reference the depth to either the water surface, or the keel.  Positive
    # values imply that the correction is for water surface to transducer; negative implies transducer to keel
    offset: float = None
    # Maximum range of observation, metres
    range: float = None

    packet_id: int = PacketTypes.Depth.value.get("id")

    # Provide the fixed-text string name for this data packet
    #
    # This simply reports the human-readable name for the class so that reporting is possible
    #
    # \return String with the human-readable name of the packet
    @staticmethod
    def name():
        return PacketTypes.Depth.value.get("name")

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for the standard streaming output interface.
    #
    # \param self   Pointer to the object
    # \return String representation of the object
    def __str__(self):
        return (
            super().__str__()
            + " "
            + self.name()
            + ": depth = "
            + str(self.depth)
            + "m, offset = "
            + str(self.offset)
            + "m, range = "
            + str(self.range)
            + "m"
        )


# Implement the Course-over-Ground Rapid NMEA2000 message
#
# The Course-over-ground/Speed-over-ground message is sent more frequently that most, and contains estimates of the
# current course and speed.
@dataclass
class COG(DataPacket, DataClassJSONMixin):
    # Course over ground (radians)
    course_over_ground: float = None
    # Speed over ground (m/s)
    speed_over_ground: float = None

    packet_id: int = PacketTypes.COG.value.get("id")

    # Provide the fixed-text string name for this data packet
    #
    # This simply reports the human-readable name for the class so that reporting is possible
    #
    # \return String with the human-readable name of the packet
    @staticmethod
    def name():
        return PacketTypes.COG.value.get("name")

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for the standard streaming output interface.
    #
    # \return String representation of the object
    def __str__(self):
        return (
            super().__str__()
            + " "
            + self.name()
            + ": course over ground = "
            + str(angle_to_degrees(self.course_over_ground))
            + " deg, speed over ground = "
            + str(self.speed_over_ground)
            + " m/s"
        )


# Implement the GNSS observation NMEA2000 message
#
# The GNSS observation message contains a single GNSS observation from a receiver on the ship (multiple receivers are
# possible, of course).  This contains all of the usual suspects that would come from a GPGGA message in NMEA0183, but
# has better information on correctors, and methods of correction, which are preserved here.
@dataclass
class GNSS(DataPacket, DataClassJSONMixin):
    # In-message date (days since epoch)
    msg_date: int = None
    # In-message timestamp (seconds since midnight)
    msg_timestamp: float = None
    # Latitude of position, degrees
    latitude: float = None
    # Longitude of position, degrees
    longitude: float = None
    # Altitude of position, metres
    altitude: float = None
    # GNSS receiver type (e.g., GPS, GLONASS, Beidou, Galileo, and some combinations)
    receiver_type: int = None
    # GNSS receiver method (e.g., C/A, Differential, Float/fixed RTK, etc.)
    receiver_method: int = None
    # Number of SVs in view
    sv_num: int = None
    # Horizontal dilution of precision (unitless)
    horizontal_dop: float = None
    # Position dilution of precision (unitless)
    position_dop: float = None
    # Geoid-ellipsoid separation, metres (modeled)
    separation: float = None
    # Number of reference stations used in corrections
    ref_station_num: int = None
    # Reference station receiver type (as for receiverType)
    ref_station_type: int = None
    # Reference station ID number
    ref_station_id: int = None
    # Age of corrections, seconds
    correction_age: float = None

    packet_id: int = PacketTypes.GNSS.value.get("id")

    # Provide the fixed-text string name for this data packet
    #
    # This simply reports the human-readable name for the class so that reporting is possible
    #
    # \return String with the human-readable name of the packet
    @staticmethod
    def name():
        return PacketTypes.GNSS.value.get("name")

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for the standard streaming output interface.
    #
    # \param self   Pointer to the object
    # \return String representation of the object
    def __str__(self):
        return (
            super().__str__()
            + " "
            + self.name()
            + ": in-message date = "
            + str(self.msg_date)
            + " days, "
            + "in-message time = "
            + str(self.msg_timestamp)
            + " s.,  latitude = "
            + str(self.latitude)
            + " deg, longitude = "
            + str(self.longitude)
            + " deg, altitude = "
            + str(self.altitude)
            + " m, GNSS type = "
            + str(self.receiver_type)
            + ", GNSS method = "
            + str(self.receiver_method)
            + ", num. SVs = "
            + str(self.sv_num)
            + ", horizontal DOP = "
            + str(self.horizontal_dop)
            + ", position DOP = "
            + str(self.position_dop)
            + ", Geoid separation = "
            + str(self.separation)
            + "m, number of ref. stations = "
            + str(self.ref_station_num)
            + ", ref. station type = "
            + str(self.ref_station_type)
            + ", ref. station ID = "
            + str(self.ref_station_id)
            + ", correction age = "
            + str(self.correction_age)
        )


# Implement the Environment NMEA2000 message
#
# The Environment message was originally used to provide a combination of temperature, humidity, and pressure, but has
# since been deprecated in favour of individual messages (which also have the benefit of preserving the source
# information for the pressure data).  These are also supported, but this is provided for backwards compatibility.
@dataclass
class Environment(DataPacket, DataClassJSONMixin):
    # Source of temperature information (e.g., inside, outside)
    # todo: confirm type
    temperature_source: int = None
    # Current temperature, Kelvin
    # todo: confirm type
    temperature: float = None
    # Source of humidity information (e.g., inside, outside)
    # todo: confirm type
    humidity_source: int = None
    # Relative humidity, percent
    # todo: confirm type
    humidity: float = None
    # Current pressure, Pascals.
    # The source information for pressure information is not provided, so presumably this is meant to be
    # atmospheric pressure, rather than something more general.
    # todo: confirm type
    pressure: float = None

    packet_id: int = PacketTypes.Environment.value.get("id")

    # Provide the fixed-text string name for this data packet
    #
    # This simply reports the human-readable name for the class so that reporting is possible
    #
    # \return String with the human-readable name of the packet
    @staticmethod
    def name():
        return PacketTypes.Environment.value.get("name")

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for the standard streaming output interface.
    #
    # \param self   Pointer to the object
    # \return String representation of the object
    def __str__(self):
        return (
            super().__str__()
            + " "
            + self.name()
            + ": temperature = "
            + str(temp_to_celsius(self.temperature))
            + " ºC (source "
            + str(self.temperature_source)
            + "), humidity = "
            + str(self.humidity)
            + "% (source "
            + str(self.humidity_source)
            + "), pressure = "
            + str(pressure_to_millibars(self.pressure))
            + " mBar"
        )


# Implement the Temperature NMEA2000 message
#
# The Temperature message can serve a number of purposes, carrying temperature information for a variety of different
# sensors on the ship, including things like bait tanks and reefers.  The information is, however, always qualified
# with a source designator.  Some filtering of messages might happen at the logger, however, which means that not all
# temperature messages make it to here.
@dataclass
class Temperature(DataPacket, DataClassJSONMixin):
    # Source of temperature information (e.g., water, air, cabin)
    # todo: confirm type
    temperature_source: int = None
    # Temperature of source, Kelvin
    # todo: confirm type
    temperature: float = None

    packet_id: int = PacketTypes.Temperature.value.get("id")

    # Provide the fixed-text string name for this data packet
    #
    # This simply reports the human-readable name for the class so that reporting is possible
    #
    # \return String with the human-readable name of the packet
    @staticmethod
    def name():
        return PacketTypes.Temperature.value.get("name")

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for the standard streaming output interface.
    #
    # \param self   Pointer to the object
    # \return String representation of the object
    def __str__(self):
        return (
            super().__str__()
            + " "
            + self.name()
            + ": temperature = "
            + str(temp_to_celsius(self.temperature))
            + " ºC (source "
            + str(self.temperature_source)
            + ")"
        )


# Implement the Humidity NMEA2000 message
#
# The Humidity message can serve a number of purposes, carrying humidity information for a variety of different sensors
# on the ship, including interior and exterior.  The information is, however, always qualified with a source designator.
# Some filtering of messages might happen at the logger, however, which means that not all humidity messages make it
# to here.
@dataclass
class Humidity(DataPacket, DataClassJSONMixin):
    # Source of humidity (e.g., inside, outside)
    # todo: confirm type
    humidity_source: int = None
    # Humidity observation, percent
    # todo: confirm type
    humidity: float = None

    packet_id: int = PacketTypes.Humidity.value.get("id")

    # Provide the fixed-text string name for this data packet
    #
    # This simply reports the human-readable name for the class so that reporting is possible
    #
    # \return String with the human-readable name of the packet
    @staticmethod
    def name():
        return PacketTypes.Humidity.value.get("name")

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for the standard streaming output interface.
    #
    # \param self   Pointer to the object
    # \return String representation of the object
    def __str__(self):
        return (
            super().__str__()
            + " "
            + self.name()
            + ": humidity = "
            + str(self.humidity)
            + " % (source "
            + str(self.humidity_source)
            + ")"
        )


# Implement the Pressure NMEA2000 message
#
# The Pressure message can serve a number of purposes, carrying pressure information for a variety of different sensors
# on the ship, including atmospheric and compressed air systems.  The information is, however, always qualified with a
# source designator.  Some filtering of messages might happen at the logger, however, which means that not all pressure
# messages make it to here.
@dataclass
class Pressure(DataPacket, DataClassJSONMixin):
    # Source of pressure measurement (e.g., atmospheric, compressed air)
    # todo: confirm type
    pressure_source: int = None
    # Pressure, Pascals
    # todo: confirm type
    pressure: float = None

    packet_id: int = PacketTypes.Pressure.value.get("id")

    # Provide the fixed-text string name for this data packet
    #
    # This simply reports the human-readable name for the class so that reporting is possible
    #
    # \return String with the human-readable name of the packet
    @staticmethod
    def name():
        return PacketTypes.Pressure.value.get("name")

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for the standard streaming output interface.
    #
    # \param self   Pointer to the object
    # \return String representation of the object
    def __str__(self):
        return (
            super().__str__()
            + " "
            + self.name()
            + ": pressure = "
            + str(pressure_to_millibars(self.pressure))
            + " mBar (source "
            + str(self.pressure_source)
            + ")"
        )


# Implement the NMEA0183 serial data message
#
# As an extension, the logger can (if the hardware is populated) record data from two separate RS-422 NMEA0183
# data streams, and timestamp in the same manner as the rest of the data.  The code encapsulates the entire message
# in this packet, rather than trying to have a separate packet for each data string type (at least for now).
@dataclass
class SerialString(DataPacket, DataClassJSONMixin):
    # Serial data encapsulated in the packet
    data: bytes = None

    packet_id: int = PacketTypes.SerialString.value.get("id")

    # Provide the fixed-text string name for this data packet
    #
    # This simply reports the human-readable name for the class so that reporting is possible
    #
    # \return String with the human-readable name of the packet
    @staticmethod
    def name():
        return PacketTypes.SerialString.value.get("name")

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for the standard streaming output interface.
    #
    # \param self   Pointer to the object
    # \return String representation of the object
    def __str__(self):
        return super().__str__() + " " + self.name() + ": payload = " + str(self.data)


# Implement the motion sensor data packet
#
# This picks out the information from the on-board motion sensor (if available). This data is not processed
# (e.g., with a Kalman filter) and may need further work before being useful.
@dataclass
class Motion(DataPacket, DataClassJSONMixin):
    # The acceleration vector, 3D
    # todo: confirm type
    acceleration: Tuple[float, float, float] = None
    # The gyroscope rate vector, 3D
    # todo: confirm type
    gyroscope_rate: Tuple[float, float, float] = None
    # Die temperature of the motion sensor
    # todo: confirm type
    temperature: float = None

    packet_id: int = PacketTypes.Motion.value.get("id")

    # Provide the fixed-text string name for this data packet
    #
    # This simply reports the human-readable name for the class so that reporting is possible
    #
    # \return String with the human-readable name of the packet
    @staticmethod
    def name():
        return PacketTypes.Motion.value.get("name")

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for the standard streaming output interface.
    #
    # \param self   Pointer to the object
    # \return String representation of the object
    def __str__(self):
        return (
            super().__str__()
            + " "
            + self.name()
            + ": acc = "
            + str(self.acceleration)
            + ", gyro = "
            + str(self.gyroscope_rate)
            + ", temp = "
            + str(self.temperature)
        )


# Implement the metadata packet
#
# This picks out the information from the metadata packet, which gives identification
# information for the logger that created the file.
@dataclass
class Metadata(DataPacket, DataClassJSONMixin):
    logger_name: str = None
    ship_name: str = None

    packet_id: int = PacketTypes.Metadata.value.get("id")

    # Provide the fixed-text string name for this data packet
    #
    # This simply reports the human-readable name for the class so that reporting is possible
    #
    # \return String with the human-readable name of the packet
    @staticmethod
    def name() -> str:
        return PacketTypes.Metadata.value.get("name")

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for the standard streaming output interface
    #
    # \param self   Pointer to the object
    # \return String representation of the object
    def __str__(self):
        return (
            DataPacket.__str__(self)
            + " "
            + self.name()
            + ": logger name = "
            + self.logger_name
            + ", identifier = "
            + self.ship_name
        )


# Implement the algorithm packet
#
# This picks out the information from the algorithm request packet, which provides an algorithm name
# and parameter set that the logger would recommend running on the data in the cloud, if available
@dataclass
class AlgorithmRequest(DataPacket, DataClassJSONMixin):
    # todo: document
    # todo: confirm type
    algorithm: str = None

    # todo: document
    # todo: confirm type
    parameters: str = None

    packet_id: int = PacketTypes.AlgorithmRequest.value.get("id")

    # Provide the fixed-text string name for this data packet
    #
    # This simply report the human-readable name for the class so that reporting is possible
    #
    # \return String with the human-readable name of the packet
    @staticmethod
    def name():
        return PacketTypes.AlgorithmRequest.value.get("name")

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for the standard streaming output interface
    #
    # \param self   Pointer to the object
    # \return String representation of the object
    def __str__(self):
        return (
            DataPacket.__str__(self)
            + " "
            + self.name()
            + ": algorithm = "
            + str(self.algorithm)
            + ", parameters = "
            + str(self.parameters)
        )


# Implement the JSON metadata packet
#
# This picks out information on metadata elements that the logger would like to send into the JSON
# file being constructed for each data file being transmitted to the database.  This is provided by
# the user and cached on the logger, and then transmitted as is, without interpretation.
@dataclass
class JSONMetadata(DataPacket, DataClassJSONMixin):
    # todo: document
    # todo: confirm type
    metadata: str = None

    packet_id: int = PacketTypes.JSONMetadata.value.get("id")

    # Provide the fixed-text string name for this data packet
    #
    # This simply reports the human-readable name for the class so that reporting is possible
    #
    # \return String with the human-readable name of the packet
    @staticmethod
    def name():
        return PacketTypes.JSONMetadata.value.get("name")

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for the standard streaming output interface
    #
    # \param self
    # \return String representation of the object
    def __str__(self):
        return (
            DataPacket.__str__(self)
            + " "
            + self.name()
            + ': metadata element = "'
            + self.metadata
            + '"'
        )


# Implement a packet to hold information on NMEA0183 packets being recorded
#
# The logger has the ability to filter the NMEA0183 sentences that are received so that it only records to
# SD card those that are of interest. Getting the filtering right can be important to let the capture run
# for as long as possible.
@dataclass
class NMEA0183Filter(DataPacket, DataClassJSONMixin):
    # todo: document
    # todo: confirm type
    recognition_string: str = None

    packet_id: int = PacketTypes.NMEA0183Filter.value.get("id")

    # Provide the fixed-text string name for this data packet
    #
    # This simply reports the human-readable name for the class so that reporting is possible
    #
    # \return String with the name of the object
    @staticmethod
    def name() -> str:
        return PacketTypes.NMEA0183Filter.value.get("name")

    # Implement the printable interface for this class, allowing it to be streamed
    #
    # This converts to human-readable version of the data packet for standard streaming output interface
    #
    # \param self   Reference for the object
    # \return String representation of the object
    def __str__(self) -> str:
        return (
            DataPacket.__str__(self)
            + " "
            + self.name()
            + ': sentence recognition string = "'
            + self.recognition_string
            + '"'
        )


Packet = Union[
    SerializerVersion,
    SystemTime,
    Attitude,
    Depth,
    COG,
    GNSS,
    Environment,
    Temperature,
    Humidity,
    Pressure,
    SerialString,
    Motion,
    Metadata,
    AlgorithmRequest,
    JSONMetadata,
    NMEA0183Filter,
]


@dataclass
class WiblObject(DataClassJSONMixin):
    logger_name: str = None
    platform: str = None
    logger_version: LoggerVersion = None
    metadata: Dict = None
    algorithms: List[Algorithm] = field(default_factory=list)
    packets: List[
        Union[
            SerializerVersion,
            SystemTime,
            Attitude,
            Depth,
            COG,
            GNSS,
            Environment,
            Temperature,
            Humidity,
            Pressure,
            SerialString,
            Motion,
            Metadata,
            AlgorithmRequest,
            JSONMetadata,
            NMEA0183Filter,
        ]
    ] = field(default_factory=list, metadata={"deserialize": deserialize_packets})
    time_source: TimeSource = None
    lineage: List[LineageEntry] = field(default_factory=list)
