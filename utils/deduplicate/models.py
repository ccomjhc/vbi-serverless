from dataclasses import dataclass, field
from typing import Union

from mashumaro.mixins.json import DataClassJSONMixin

from utils.models import LineageEntry


@dataclass
class AlgorithmLineageEntry(LineageEntry, DataClassJSONMixin):
    type: str = field(default="algorithm")
    detail: "AlgorithmLineageEntryDetails" = field(default=None)


@dataclass
class AlgorithmLineageEntryDetails(DataClassJSONMixin):
    name: str
    parameters: str
    comment: Union[str, None]

