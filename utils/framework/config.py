import json
from dataclasses import dataclass, field
from os import path
from typing import Union, Dict

from mashumaro.mixins.json import DataClassJSONMixin

from utils.aws.s3 import get_object


@dataclass
class DeploymentConfigSection(DataClassJSONMixin):
    stage: str = None
    region: str = None


@dataclass
class StorageConfigSection(DataClassJSONMixin):
    bucket_name: str = None


@dataclass
class InstanceConfig(DataClassJSONMixin):
    verbose: bool
    deployment: DeploymentConfigSection
    storage: StorageConfigSection


@dataclass
class Config(DataClassJSONMixin):
    instance: InstanceConfig
    job: Dict[str, dict] = field(default_factory=dict)

    def add_job_config(self, job_name: str) -> None:
        config: Union[dict, None] = load_job_config(
            job_name=job_name, stage=self.instance.deployment.stage
        )

        if config is None:
            raise FileNotFoundError(
                f"Unable to locate config for job with name '{job_name}' and stage '{self.instance.deployment.stage}'."
            )

        if self.job.get(job_name) is not None:
            print(
                f"Config for job with name '{job_name}' has already been added, skipping."
            )

        self.job[job_name] = config


def load_config(bucket: str) -> Config:
    instance_config: InstanceConfig = load_instance_config(bucket=bucket)

    config: Config = Config(instance=instance_config)

    return config


def load_instance_config(bucket: str) -> InstanceConfig:
    instance_config = get_object(bucket=bucket, key="cfg/config.instance.json")

    if instance_config is None:
        raise FileNotFoundError("Unable to locate instance config.")

    return InstanceConfig.from_dict(json.loads(instance_config))


def load_job_config(job_name: str, stage: str) -> Union[dict, None]:
    project_path: str = str(path.join(path.abspath(path.dirname(__file__)), "../../"))

    config_path = "/{0}/functions/jobs/{1}/config.{2}.json".format(
        project_path, job_name, stage
    )

    if not path.exists(config_path):
        return None

    with open(config_path, "r") as f:
        # todo(question): what happens if file is not valid json?
        config = json.loads(f.read())

    return config
