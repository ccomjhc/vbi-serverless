import uuid
from dataclasses import dataclass, field
from typing import List, Union, Optional, Dict

from mashumaro.mixins.json import DataClassJSONMixin

from utils.aws.s3 import (
    put_object,
    S3ObjectPutException,
    S3ObjectCreationException,
    create_directory,
    get_object,
    download_file,
)
from utils.framework.config import Config


@dataclass
class Version(DataClassJSONMixin):
    major: int
    minor: int
    patch: int

    def __post_init__(self):
        if self.major < 0:
            raise ValueError("major < 0")
        if self.minor < 0:
            raise ValueError("minor < 0")
        if self.patch < 0:
            raise ValueError("patch < 0")

    def __str__(self):
        return "{0}.{1}.{2}".format(self.major, self.minor, self.patch)


class TooManyOptionsException(Exception):
    pass


@dataclass
class Artifact(DataClassJSONMixin):
    key: str
    object_key: str


@dataclass
class Job(DataClassJSONMixin):
    name: str
    version: Version
    step_id: Union[int, None] = None
    artifacts: Dict[str, Artifact] = field(default_factory=dict)

    def add_artifact(self, artifact: Artifact) -> None:
        self.artifacts[artifact.key] = artifact

    def remove_artifact(self, key: str) -> bool:
        artifact: [Artifact, None] = self.get_artifact(key)

        if artifact is None:
            return False
        else:
            self.artifacts.pop(key)
            return True

    def get_artifact(self, key: str) -> Union[Artifact, None]:
        return self.artifacts.get(key)

    def __str__(self):
        if self.step_id is None:
            return "{0}-{1}".format(self.name, self.version)
        else:
            return "{0}-{1}-{2}".format(self.step_id, self.name, self.version)


class UnregisteredJobException(Exception):
    pass


@dataclass
class Pipeline(DataClassJSONMixin):
    config: Config
    id: str = field(default=str(uuid.uuid4()))
    jobs: List[Job] = field(default_factory=list)
    next_step: str = field(default="job.init")
    next_step_id: int = field(default=0)
    current_step_id: int = field(default=0)

    def __post_init__(self):
        key = "cache/{0}/".format(self.id)
        bucket = self.config.instance.storage.bucket_name

        response = create_directory(bucket=bucket, key=key)

        if not response:
            raise S3ObjectCreationException(
                "Unable to create pipeline cache directory '{0}'".format(key)
            )

    def add_artifact(self, job: Job, artifact: Artifact, data: bytes):
        # When a job is registered with the pipeline, it is given a step_id. This property enables a job to be re-run
        #       while continuing to have a unique id.
        if job.step_id is None:
            raise UnregisteredJobException()

        bucket = self.config.instance.storage.bucket_name

        # cache/{{pipeline_id}}/{{job_step_id}}-{{job_name}}-{{job_version}}/{{artifact_object_key}}
        key = "cache/{0}/{1}/{2}".format(self.id, job, artifact.object_key)

        response = put_object(
            bucket=bucket,
            key=key,
            data=data,
        )

        if not response:
            raise S3ObjectPutException(
                "Unable to put artifact '{0}' to bucket '{1}'".format(
                    artifact.object_key, bucket
                )
            )

        job.add_artifact(artifact)

    def get_artifact(self, job: Job, artifact_key: str) -> Optional[bytes]:
        bucket = self.config.instance.storage.bucket_name
        artifact: Optional[Artifact] = None

        artifact = job.get_artifact(artifact_key)

        if artifact is None:
            return None

        # cache/{{pipeline_id}}/{{job_step_id}}-{{job_name}}-{{job_version}}/{{artifact_object_key}}
        key = "cache/{0}/{1}/{2}".format(self.id, job, artifact.object_key)

        return get_object(bucket=bucket, key=key)

    def download_artifact(
        self, job: Job, artifact_key: str, dest_path: str
    ) -> [bool, Union[str, None]]:
        bucket = self.config.instance.storage.bucket_name
        artifact = job.get_artifact(artifact_key)

        if artifact is None:
            return None

        # cache/{{pipeline_id}}/{{job_step_id}}-{{job_name}}-{{job_version}}/{{artifact_object_key}}
        key = "cache/{0}/{1}/{2}".format(self.id, job, artifact.object_key)

        return download_file(bucket=bucket, key=key, dest_path=dest_path)

    def register_job(self, job: Job, has_config: bool = False) -> None:
        job.step_id = self.next_step_id

        bucket = self.config.instance.storage.bucket_name
        key = "cache/{0}/{1}/".format(self.id, job)
        response = create_directory(bucket=bucket, key=key)

        if not response:
            raise S3ObjectCreationException(
                "Unable to create job cache directory '{0}'".format(key)
            )

        if has_config:
            self.config.add_job_config(job_name=job.name)

        self.next_step_id += 1
        self.jobs.append(job)

    def unregister_job(self, name: str, version: Version = None) -> bool:
        job: [Job, None] = self.get_job(name, version)

        try:
            self.jobs.remove(job)
            return True
        except ValueError:
            return False

    def get_job(self, name: str, version: Version = None) -> [Job, None]:
        name_match_jobs: List[Job] = []

        for job in self.jobs:
            if version is None:
                if job.name == name:
                    name_match_jobs.append(job)
            elif job.name == name and job.version == version:
                return job

        if version is None or len(name_match_jobs) == 0:
            return None

        if len(name_match_jobs) > 1:
            raise TooManyOptionsException(
                "Multiple job version registered when no job version specified."
            )

        return name_match_jobs[0]

    def get_last_job(self) -> Job:
        index: int = self.current_step_id - 1

        if index < 0:
            raise IndexError("Index out of range when fetching last job.")

        return self.jobs[index]

    def get_current_job(self) -> Job:
        index: int = self.current_step_id
        return self.jobs[index]


@dataclass
class Event(DataClassJSONMixin):
    source: str
    pipeline: Pipeline
