from abc import ABC
from dataclasses import dataclass, field
from datetime import datetime
from typing import Dict

from mashumaro.mixins.json import DataClassJSONMixin


@dataclass
class LineageEntry(ABC, DataClassJSONMixin):
    type: str = field(default=None)
    detail: Dict = field(default=None)
    timestamp: int = field(default=datetime.now().isoformat())
