import pickle

pickle_serialization_strategy = {"serialize": pickle.dumps, "deserialize": pickle.loads}
