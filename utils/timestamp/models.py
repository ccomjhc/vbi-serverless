from dataclasses import dataclass, field
from typing import List

import numpy
from mashumaro.mixins.json import DataClassJSONMixin

from utils.decode.models import WiblObject
from utils.serialization import pickle_serialization_strategy


@dataclass
class Depth(DataClassJSONMixin):
    t: numpy.ndarray = field(metadata=pickle_serialization_strategy)
    lat: List[numpy.ndarray] = field(metadata=pickle_serialization_strategy)
    lon: List[numpy.ndarray] = field(metadata=pickle_serialization_strategy)
    z: numpy.ndarray = field(metadata=pickle_serialization_strategy)


@dataclass
class Heading(DataClassJSONMixin):
    t: numpy.ndarray = field(metadata=pickle_serialization_strategy)
    lat: List[numpy.ndarray] = field(metadata=pickle_serialization_strategy)
    lon: List[numpy.ndarray] = field(metadata=pickle_serialization_strategy)
    heading: numpy.ndarray = field(metadata=pickle_serialization_strategy)


@dataclass
class WaterTemperature(DataClassJSONMixin):
    t: numpy.ndarray = field(metadata=pickle_serialization_strategy)
    lat: List[numpy.ndarray] = field(metadata=pickle_serialization_strategy)
    lon: List[numpy.ndarray] = field(metadata=pickle_serialization_strategy)
    temperature: numpy.ndarray = field(metadata=pickle_serialization_strategy)


@dataclass
class Wind(DataClassJSONMixin):
    t: numpy.ndarray = field(metadata=pickle_serialization_strategy)
    lat: List[numpy.ndarray] = field(metadata=pickle_serialization_strategy)
    lon: List[numpy.ndarray] = field(metadata=pickle_serialization_strategy)
    direction: numpy.ndarray = field(metadata=pickle_serialization_strategy)
    speed: numpy.ndarray = field(metadata=pickle_serialization_strategy)


@dataclass
class TimestampedWiblObject(DataClassJSONMixin):
    wiblobject: WiblObject = field(default=None)
    depth: Depth = field(default=None)
    heading: Heading = field(default=None)
    water_temperature: WaterTemperature = field(default=None)
    wind: Wind = field(default=None)
