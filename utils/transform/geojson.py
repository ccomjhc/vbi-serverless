# \file GeoJSONConvert.py
# \brief Convert the interpolated data from the logger into correctly formatted GeoJSON data for DCDB
#
# The data logger provides data in binary format, and the Timestamping.py module reads that format
# (using LoggerFile.py) and then fixes the timestamps, end up with a dictionary that contains the
# reference times for each depth measurement, along with the depth in metres and position in latitude
# and longitude. The code here converts this to the GeoJSON format required by DCDB for submission.
#
# This code is based on work done by Taylor Roy et al., as part of a UNH Computer Science undergraduate
# programme final year project, and then modified for slightly better maintainability.

from datetime import datetime
from typing import Dict, Any

from utils.decode.models import WiblObject
from utils.timestamp.models import TimestampedWiblObject


def to_geojson(
    provider_id: str, timestamped_wiblobject: TimestampedWiblObject
) -> Dict[str, Any]:
    # Original comment:
    # geojson formatting - Taylor Roy
    # based on https://ngdc.noaa.gov/ingest-external/#_testing_csb_data_submissions example geojson

    wiblobject: WiblObject = timestamped_wiblobject.wiblobject

    features = []

    for i in range(len(timestamped_wiblobject.depth.z)):
        timestamp = datetime.fromtimestamp(
            timestamped_wiblobject.depth.t[i]
        ).isoformat()

        feature: dict = {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    timestamped_wiblobject.depth.lon[i],
                    timestamped_wiblobject.depth.lat[i],
                ],
            },
            "properties": {
                "depth": timestamped_wiblobject.depth.z[i],
                "time": timestamp,
            },
        }

        features.append(feature)

    feature_collection: dict = {
        "type": "FeatureCollection",
        "crs": {"type": "name", "properties": {"name": "EPSG:4326"}},
        "properties": {
            "convention": "CSB 2.0",
            "platform": {
                "uniqueID": bytes(wiblobject.logger_name).decode("utf-8"),
                "type": "Ship",
                "name": bytes(wiblobject.platform).decode("utf-8"),
                "IDType": "LoggerName",
                "IDNumber": bytes(wiblobject.logger_name).decode("utf-8"),
            },
            "providerContactPoint": {
                "orgName": "CCOM/JHC, UNH",
                "email": "wibl@ccom.unh.edu",
                "logger": "WIBL",
                "loggerVersion": str(wiblobject.logger_version),
            },
            "depthUnits": "meters",
            "timeUnits": "ISO 8601",
        },
        "lineage": [],
        "features": features,
    }

    if wiblobject.metadata is not None:
        meta = wiblobject.metadata

        if "platform" in meta:
            feature_collection["properties"]["platform"] = meta["platform"]
        if "providerContactPoint" in meta:
            feature_collection["properties"]["providerContactPoint"] = meta[
                "providerContactPoint"
            ]
        if "depthUnits" in meta:
            feature_collection["properties"]["depthUnits"] = meta["depthUnits"]
        if "timeUnits" in meta:
            feature_collection["properties"]["timeUnits"] = meta["timeUnits"]

    # The database requires that the unique ID contains the provider's ID, presumably to avoid
    # namespace clashes. We therefore check now (after the platform metadata is finalised) to make
    # sure that this is the case.
    if provider_id not in feature_collection["properties"]["platform"]["uniqueID"]:
        feature_collection["properties"]["platform"]["uniqueID"] = (
            provider_id + "-" + feature_collection["properties"]["platform"]["uniqueID"]
        )
    if len(wiblobject.algorithms) > 0:
        feature_collection["properties"]["algorithms"] = wiblobject.algorithms
    if wiblobject.lineage is not None:
        feature_collection["lineage"] = wiblobject.lineage

    return feature_collection
